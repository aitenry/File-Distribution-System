CREATE SCHEMA `file_distribution_sys` ;

CREATE TABLE `file_distribution_sys`.`users` (
  `username` VARCHAR(15) NOT NULL,
  `password` VARCHAR(15) NOT NULL,
  `online` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`username`));

CREATE TABLE `file_distribution_sys`.`admin` (
  `username` VARCHAR(15) NOT NULL,
  `password` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`username`));

CREATE TABLE `file_distribution_sys`.`users_upload` (
  `username` VARCHAR(15) NOT NULL,
  `file_name` VARCHAR(45) NOT NULL,
  `upload_time` DATETIME NOT NULL,
  `address` VARCHAR(200) NOT NULL,
  `file_size` BIGINT NOT NULL,
  PRIMARY KEY (`username`, `file_name`, `upload_time`));

CREATE TABLE `file_distribution_sys`.`admin_upload` (
  `username` VARCHAR(15) NOT NULL,
  `file_name` VARCHAR(45) NOT NULL,
  `upload_time` DATETIME NOT NULL,
  `address` VARCHAR(200) NOT NULL,
  `file_size` BIGINT NOT NULL,
  PRIMARY KEY (`username`, `file_name`, `upload_time`));

ALTER TABLE `file_distribution_sys`.`users` ADD CONSTRAINT ck_user_online CHECK (`online` = 0 or `online` = 1);

ALTER TABLE `file_distribution_sys`.`users_upload` 
ADD CONSTRAINT `fk_users_upload_username`
  FOREIGN KEY (`username`)
  REFERENCES `file_distribution_sys`.`users` (`username`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `file_distribution_sys`.`admin_upload` 
ADD CONSTRAINT `fk_admin_upload_username`
  FOREIGN KEY (`username`)
  REFERENCES `file_distribution_sys`.`admin` (`username`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

INSERT INTO `file_distribution_sys`.`users` VALUES
('雨露沾青衫', 'Users06222020', 1),
('花落满人间', 'Users07012020', 0),
('Bruce Wayne', 'Users11272023', 1),
('Lucas Alexander', 'Users12032019', 1),
('南絮', 'Users05202019', 0),
('Jane Smith', 'Users07252022', 1),
('Pablo Gomez', 'Users10272020', 1),
('青石巷', 'Users03162019', 0),
('Juan Hernandez', 'Users08282021', 1),
('Miguel Garcia', 'Users11242018', 1),
('Emily Johnson', 'Users04032020', 0),
('林下月疏', 'Users02122023', 1),
('Olivia Kim', 'Users11112022', 1),
('人烟寒橘柚', 'Users08152019', 0),
('Daniel Wong', 'Users05012021', 1),
('David Liu', 'Users02222020', 1),
('奈南墙冷落', 'Users10032018', 0),
('Tom Smith', 'Users04122023', 1),
('Jerry Chen', 'Users07202021', 1),
('Amy Wang', 'Users02202022', 0),
('素衣叹风尘', 'Users04242019', 1),
('Hao Zhang', 'Users10302020', 1),
('似黄粱梦', 'Users06012018', 0),
('Peter Li', 'Users08082022', 1),
('缓步香茵', 'Users01292023', 1),
('Tony Guo', 'Users12122017', 0),
('Karen Chen', 'Users03062021', 1);

INSERT INTO `file_distribution_sys`.`admin` VALUES
('Root', 'Admin03172001');