module com.aiten.fds {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    exports com.aiten.fds.ui;
}