package com.aiten.fds.utils;

import java.io.File;
import java.util.List;

import com.aiten.fds.dao.UserDAO;
import com.aiten.fds.domain.User;

public class DataHandleUtils {

	/**
	 * 根据类型、用户名和文件名生成文件路径。
	 * @param type 类型（admin或users）
	 * @param username 用户名（仅适用于用户类型）
	 * @param fileName 文件名
	 * @return 文件路径字符串，如果类型不是admin或users，则返回null，反之返回会对应的存储路径
	 */
	public static String getFileRoute(String type, String username, String fileName) {
		if ("admin".equals(type)) {
			return String.format("src/main/resources/warehouse/admin/%s", fileName);
		} else if ("users".equals(type)) {
			return String.format("src/main/resources/warehouse/users/%s/%s", username, fileName);
		}
		return null;
	}

	/**
	 * 将文件大小从字节转换为更容易阅读的格式。
	 *
	 * @param fileSize 文件大小，单位为字节。
	 * @return 可读的文件大小字符串。
	 */
	public static String convertFileSize(long fileSize) {
		String sizeStr;
		double convertedSize;

		if (fileSize < 1024) {
			sizeStr = fileSize + " B";
		} else if (fileSize < 1024 * 1024) {
			convertedSize = (double) fileSize / 1024;
			sizeStr = String.format("%.2f KB", convertedSize);
		} else if (fileSize < 1024 * 1024 * 1024) {
			convertedSize = (double) fileSize / (1024 * 1024);
			sizeStr = String.format("%.2f MB", convertedSize);
		} else {
			convertedSize = (double) fileSize / (1024 * 1024 * 1024);
			sizeStr = String.format("%.2f GB", convertedSize);
		}

		return sizeStr;

	}

	/**
	 * 初始化用户文件夹。如果文件夹不存在，则创建文件夹。
	 */
	public static void initializeFolder() {
		List<User> users = new UserDAO().getUserList();
		String filePath;
		for (User user : users) {
			filePath = "src/main/resources/warehouse/users/" + user.getUsername();
			File folder = new File(filePath);
			if (!folder.exists() && !folder.isDirectory()) {
				if(!folder.mkdirs()) {
					System.out.println("Failed to create folder: " + folder.getAbsolutePath());
				}
			}
		}
	}

}
