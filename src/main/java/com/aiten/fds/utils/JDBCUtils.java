package com.aiten.fds.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class JDBCUtils {

    private static final String url;
    private static final String username;
    private static final String password;
    private static final String driverClass;

    private static final String FILENAME = "/config/dbInfo.properties";

    static {
        // 加载配置文件信息
        Properties props = new Properties();
        InputStream is = null;
        try {
            is = JDBCUtils.class.getResourceAsStream(FILENAME);
            if (is == null) {
                throw new FileNotFoundException("Unable to find " + FILENAME);
            }
            props.load(is);

            url = props.getProperty("jdbc.url");
            username = props.getProperty("jdbc.username");
            password = props.getProperty("jdbc.password");
            driverClass = props.getProperty("jdbc.driverClass");

            Class.forName(driverClass);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * 获取数据库连接对象
     *
     * @return Connection
     */
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    /**
     * 关闭JDBC连接资源
     *
     * @param conn 连接
     * @param st Statement对象
     * @param rs 结果集
     */
    public static void closeResource(Connection conn, Statement st, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭JDBC连接资源
     *
     * @param conn 连接
     * @param st Statement对象
     */
    public static void closeResource(Connection conn, Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
