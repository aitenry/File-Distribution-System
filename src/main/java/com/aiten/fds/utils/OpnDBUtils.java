package com.aiten.fds.utils;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OpnDBUtils {

	/**
	 * 针对于不同的表，通用的插入、更新、删除操作，针对单个对象。
	 *
	 * @param sql  执行的sql语句
	 * @param args 占位符的可变参数
	 * @return boolean 返回是否成功插入、更新、删除了一条记录
	 */
	public static boolean createGDBO(String sql, Object... args) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = JDBCUtils.getConnection();
			ps = conn.prepareStatement(sql);
			for (int i = 0; i < args.length; i++) {
				ps.setObject(i + 1, args[i]);
			}
			int count = ps.executeUpdate();
			return count > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtils.closeResource(conn, ps);
		}
		return false;
	}

	/**
	 * 针对于不同的表，通用的批量插入数据操作。
	 *
	 * @param sql SQL语句
	 * @param dataList 存储多个映射类数据的列表
	 * @param clazz 映射类的类型
	 * @return 成功插入的行数
	 */
	public static <T> int insertBatch(Class<T> clazz, String sql, List<T> dataList) throws SQLException {
		if (dataList == null || dataList.size() == 0) {
			return 0;
		}
		Connection conn = null;
		PreparedStatement ps = null;
		int result = 0;
		try {
			// 获取数据库连接
			conn = JDBCUtils.getConnection();
			// 关闭自动提交事务
			conn.setAutoCommit(false);
			// 预编译sql
			ps = conn.prepareStatement(sql);
			for (T data : dataList) {
				// 给预编译SQL语句设置参数
				setParameters(ps, data, clazz);
				// 加入到批处理中
				ps.addBatch();
			}
			// 执行批处理
			int[] batchResult = ps.executeBatch();
			// 求和batchResult得到成功插入的行数
			for (int i : batchResult) {
				result += i;
			}
			// 提交事务
			conn.commit();
		} catch (SQLException e) {
			// 回滚事务
			assert conn != null;
			conn.rollback();
			throw e;
		} finally {
			// 释放资源
			JDBCUtils.closeResource(conn, ps);
		}
		return result;
	}

	/**
	 * 针对于不同的表的通用的查询操作，返回表中的一条记录
	 *
	 * @param clazz 运行时类是哪个类
	 * @param sql   执行的sql语句
	 * @param args  占位符的可变参数
	 * @return T 返回查询结果并封装成对象T
	 */
	public static <T> T queryForObject(Class<T> clazz, String sql, Object... args) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = JDBCUtils.getConnection();
			ps = conn.prepareStatement(sql);
			for (int i = 0; i < args.length; i++) {
				ps.setObject(i + 1, args[i]);
			}

			rs = ps.executeQuery();
			// 获取结果集的元数据 :ResultSetMetaData
			ResultSetMetaData rsmd = rs.getMetaData();
			// 通过ResultSetMetaData获取结果集中的列数
			int columnCount = rsmd.getColumnCount();

			if (rs.next()) {
				T t = clazz.getDeclaredConstructor().newInstance();
				// 处理结果集一行数据中的每一个列
				for (int i = 0; i < columnCount; i++) {
					// 获取列值
					Object columValue = rs.getObject(i + 1);

					// 获取每个列的列标签
					String columnLabel = rsmd.getColumnLabel(i + 1);

					// 给t对象指定的columnName属性，赋值为columValue：通过反射
					Field field = clazz.getDeclaredField(columnLabel);
					field.setAccessible(true);
					field.set(t, columValue);
				}
				return t;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtils.closeResource(conn, ps, rs);
		}
		return null;
	}

	/**
	 * 针对不同表的查询操作，返回多条记录并处理
	 *
	 * @param clazz 运行时类是哪个类
	 * @param sql   执行的sql语句
	 * @param args  占位符的可变参数
	 * @return List<T> 返回查询结果并封装成对象T的集合
	 */
	public static <T> List<T> queryForList(Class<T> clazz, String sql, Object... args) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = JDBCUtils.getConnection();
			ps = conn.prepareStatement(sql);
			for (int i = 0; i < args.length; i++) {
				ps.setObject(i + 1, args[i]);
			}

			rs = ps.executeQuery();
			// 获取结果集的元数据 :ResultSetMetaData
			ResultSetMetaData rsmd = rs.getMetaData();
			// 通过ResultSetMetaData获取结果集中的列数
			int columnCount = rsmd.getColumnCount();
			// 创建集合对象
			ArrayList<T> list = new ArrayList<>();
			while (rs.next()) {
				T t = clazz.getDeclaredConstructor().newInstance();
				// 处理结果集一行数据中的每一个列:给t对象指定的属性赋值
				for (int i = 0; i < columnCount; i++) {
					// 获取列值
					Object columValue = rs.getObject(i + 1);

					// 获取每个列的列标签
					String columnLabel = rsmd.getColumnLabel(i + 1);

					// 给t对象指定的columnName属性，赋值为columValue：通过反射
					Field field = clazz.getDeclaredField(columnLabel);
					field.setAccessible(true);
					field.set(t, columValue);
				}
				list.add(t);// 对于查询到的多条数据来说，把每一个对象放进集合里
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtils.closeResource(conn, ps, rs);
		}
		return null;
	}

	/**
	 * 给预编译SQL语句设置参数
	 */
	private static <T> void setParameters(PreparedStatement ps, T data, Class<T> clazz) throws SQLException {
		Field[] fields = clazz.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			Object fieldValue = null;
			try {
				// 反射获取属性值
				field.setAccessible(true);
				fieldValue = field.get(data);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			// 设置参数
			ps.setObject(i + 1, fieldValue);
		}
	}

}
