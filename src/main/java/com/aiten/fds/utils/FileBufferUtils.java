package com.aiten.fds.utils;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class FileBufferUtils {

	private static final Object LOCK = new Object();
	private static volatile BufferPool bufferPool = null;

	/**
	 * 根据文件大小选择适当的缓冲池，并从所选池中返回一个字节数组。
	 *
	 * @param fileSize 文件大小
	 * @return 缓冲区字节数组
	 */
	public static byte[] getFileBuffer(long fileSize) throws InterruptedException {
		return getBufferPool().getBuffer(fileSize);
	}

	/**
	 * 将字节数组返回到适当的缓存池中，以实现有效的归还缓冲区。
	 *
	 * @param buffer 缓冲区字节数组
	 */
	public static void returnFileBuffer(byte[] buffer) throws InterruptedException {
		getBufferPool().returnBuffer(buffer);
	}

	/**
	 * 返回单例模式的缓存池对象，线程安全
	 */
	public static BufferPool getBufferPool() {
		if (bufferPool == null) {
			synchronized (LOCK) {
				if (bufferPool == null) {
					bufferPool = new BufferPool();
				}
			}
		}
		return bufferPool;
	}

	private static class BufferPool {

		// 定义阻塞队列
		private final BlockingQueue<byte[]> minPool;
		private final BlockingQueue<byte[]> medPool;
		private final BlockingQueue<byte[]> maxPool;

		private BufferPool() {
			// 初始化最小缓存池，包含10个大小为12MB的缓冲区
			this.minPool = new ArrayBlockingQueue<>(30);
			for(int i = 0; i < 30; i++) {
				minPool.add(new byte[12 * 1024 * 1024]);
			}

			// 初始化中等缓存池，包含5个大小为17MB的缓冲区
			this.medPool = new ArrayBlockingQueue<>(15);
			for(int i = 0; i < 15; i++) {
				medPool.add(new byte[17 * 1024 * 1024]);
			}

			// 初始化最大缓存池，包含2个大小为36MB的缓冲区
			this.maxPool = new ArrayBlockingQueue<>(6);
			for(int i = 0; i < 6; i++) {
				maxPool.add(new byte[36 * 1024 * 1024]);
			}
		}

		/**
		 * 根据文件大小选择对应的缓存池，并获取缓存空间
		 *
		 * @param fileSize 文件大小
		 * @return 缓冲区字节数组
		 */
		private byte[] getBuffer(long fileSize) throws InterruptedException {
			if (fileSize <= 0) {
				return null;
			} else if (fileSize > (long) 1024 * 1024 * 1024) {
				return maxPool.take();
			} else if (fileSize > 100L * 1024 * 1024) {
				return medPool.take();
			} else {
				return minPool.take();
			}
		}

		/**
		 * 将缓存池归还到对应的缓存池中
		 *
		 * @param buffer 缓冲区字节数组
		 */
		private void returnBuffer(byte[] buffer) {
			if (buffer != null) {
				if (buffer.length == 12 * 1024 * 1024) {
					if (!minPool.offer(buffer)) {
						System.out.println("Error-buffer");
					}
				} else if (buffer.length == 17 * 1024 * 1024) {
					if (!minPool.offer(buffer)) {
						System.out.println("Error-buffer");
					}
				} else if (buffer.length == 36 * 1024 * 1024) {
					if (!minPool.offer(buffer)) {
						System.out.println("Error-buffer");
					}
				}
			}
		}
	}
}
