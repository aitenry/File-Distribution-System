package com.aiten.fds.handles.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;

public class CSCmdProcess implements Runnable {

	private static final int SERVER_PORT = 3333;
	private static final String SERVER_IP = "127.0.0.1";
	
	private boolean state;
	private final String username;
	private final String password;
	private final String command;
	
	private final CountDownLatch latch = new CountDownLatch(1);
	
	public CSCmdProcess(String username, String password, String command) {
		this.username = username;
		this.password = password;
		this.command = command;
	}

	@Override
	public void run() {
		try {
			Socket socket = new Socket(SERVER_IP, SERVER_PORT);
			System.out.println("Connected to server " + SERVER_IP + ":" + SERVER_PORT);

			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
			
			writer.println(username);
			writer.println(password);
			writer.println(command);
			
			state = Boolean.parseBoolean(reader.readLine());
			
			socket.close();
			System.out.println("Disconnected from server " + SERVER_IP + ":" + SERVER_PORT);

			latch.countDown();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
    
    public boolean getCmdState() throws InterruptedException {
    	latch.await();
        return state;
    }
    
}
