package com.aiten.fds.handles.client;

import java.io.*;
import java.net.*;
import java.util.Objects;

import com.aiten.fds.utils.FileBufferUtils;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressBar;
import com.aiten.fds.utils.DataHandleUtils;

public class CSFileUpload extends Task<Void> implements Runnable{

    private static final int SERVER_PORT = 8888;
    private static final String SERVER_IP = "127.0.0.1";
    private final String filePath;
    private final String username;
    private final String type;
    private final ProgressBar uploadFileBar;

    public CSFileUpload(String filePath, String username, String type, ProgressBar uploadFileBar) {
        this.filePath = filePath;
        this.username = username;
        this.type = type;
        this.uploadFileBar = uploadFileBar;
        updateProgress(0, 1); // 初始化进度条
    }

    @Override
    public Void call() throws Exception {
        try (Socket socket = new Socket(SERVER_IP, SERVER_PORT);
             FileInputStream fileInputStream = new FileInputStream(filePath);
             OutputStream outputStream = socket.getOutputStream();
             DataOutputStream dataOutputStream = new DataOutputStream(outputStream)) 
        {
            // 发送文件名和内容
            File file = new File(filePath);
            String fileName = file.getName();
            if (type.equals("admin")) {
            	dataOutputStream.writeUTF(String.format("src/main/resources/warehouse/admin/%s*%s", fileName, username));
            } else {
            	dataOutputStream.writeUTF(Objects.requireNonNull(DataHandleUtils.getFileRoute(type, username, fileName))); // 发送信息
            }
            
            long fileSize = file.length(); // 获取文件大小，用于计算传输进度

            dataOutputStream.writeLong(fileSize); // 发送文件长度

            byte[] buffer;
            long totalSize = 0; // 已传输数据块的总大小，用于计算传输进度

            while ((buffer = FileBufferUtils.getFileBuffer(fileSize)) != null) {
                int len = fileInputStream.read(buffer);
                if (len == -1) {
                    break;
                }
                outputStream.write(buffer, 0, len);
                FileBufferUtils.returnFileBuffer(buffer); // 归还缓冲区
                totalSize += len;

                double progress = (double) totalSize / fileSize; // 计算传输进度
                Platform.runLater(() -> uploadFileBar.setProgress(progress));
            }
            fileInputStream.close();
            System.out.println("File " + fileName + " uploaded.");

        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }
        return null;
    }

    @Override
    public void run() {
        try {
			call();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
