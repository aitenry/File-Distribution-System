package com.aiten.fds.handles.client;

import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class CRUserInfo implements Runnable {

	private static final int SERVER_PORT = 9999;
	private static final String SERVER_IP = "127.0.0.1";

	private List<String> userInfoList;

	private final CountDownLatch latch = new CountDownLatch(1);

	@Override
	public void run() {
		try {
			Socket socket = new Socket(SERVER_IP, SERVER_PORT);
			System.out.println("Connected to server " + SERVER_IP + ":" + SERVER_PORT);

			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			StringBuilder responseBuilder = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				responseBuilder.append(line).append(",");
			}

			userInfoList = Arrays.asList(responseBuilder.toString().split(","));

			socket.close();
			System.out.println("Disconnected from server " + SERVER_IP + ":" + SERVER_PORT);

			latch.countDown();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public List<String> getUserFileInfoList() throws InterruptedException {
		latch.await();
        return userInfoList;
    }
	
}
