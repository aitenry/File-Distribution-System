package com.aiten.fds.handles.client;

import java.io.*;
import java.net.*;
import java.util.concurrent.CountDownLatch;

public class CRLoginVerify implements Runnable {

	private static final int SERVER_PORT = 5555;
	private static final String SERVER_IP = "127.0.0.1";
	private final String username;
	private final String password;
	private final String type;
	private boolean verify;
	
	private final CountDownLatch latch = new CountDownLatch(1);
	
	public CRLoginVerify(String username, String password, String type) {
		this.username = username;
		this.password = password;
		this.type = type;
	}

	@Override
	public void run() {
		try {
			Socket socket = new Socket(SERVER_IP, SERVER_PORT);
			System.out.println("Connected to server " + SERVER_IP + ":" + SERVER_PORT);

			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
			
			writer.println(username);
			writer.println(password);
			writer.println(type);

			verify = Boolean.parseBoolean(reader.readLine());

			socket.close();
			System.out.println("Disconnected from server " + SERVER_IP + ":" + SERVER_PORT);

			latch.countDown();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public boolean getVerifyAns() throws InterruptedException {
		latch.await();
        return verify;
    }
	
}
