package com.aiten.fds.handles.client;

import java.io.*;
import java.net.Socket;

import com.aiten.fds.utils.FileBufferUtils;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressBar;
import com.aiten.fds.utils.DataHandleUtils;

public class CRFileDownload extends Task<Void> implements Runnable {

    private static final int SERVER_PORT = 6666;
    private static final String SERVER_IP = "127.0.0.1";
    private final String fileName;
    private final String username;
    private final String type;
    private final ProgressBar downloadFileBar;

    public CRFileDownload(String fileName, String username, String type, ProgressBar downloadFileBar) {
        this.fileName = fileName;
        this.username = username;
        this.type = type;
        this.downloadFileBar = downloadFileBar;
        updateProgress(0, 1); // 初始化进度条
    }

    @Override
    public Void call() {
    	try (Socket socket = new Socket(SERVER_IP, SERVER_PORT);
                OutputStream os = socket.getOutputStream();
                PrintWriter pw = new PrintWriter(os);
                InputStream is = socket.getInputStream();
                DataInputStream din = new DataInputStream(is);
                BufferedInputStream bis = new BufferedInputStream(is))
           {
               pw.println(DataHandleUtils.getFileRoute(type, username, fileName));
               pw.flush();

               int exist = din.readInt(); // 读取服务器的响应
               long totalSize = 0;

               if (exist == -1) {
                   System.out.println("The file does not exist!");
               } else {
                   FileOutputStream fos = new FileOutputStream("download/" + fileName);
                   byte[] buffer;

                   long fileSize = din.readLong(); // 读取文件长度

                   while ((totalSize < fileSize)) {
                       buffer = FileBufferUtils.getFileBuffer(fileSize - totalSize); // 获取缓冲区
                       int len = bis.read(buffer);
                       if (len > 0) {
                           fos.write(buffer, 0, len);
                           fos.flush();
                           totalSize += len;
                           double progress = (double) totalSize / fileSize;
                           Platform.runLater(() -> downloadFileBar.setProgress(progress));
                       }
                       FileBufferUtils.returnFileBuffer(buffer); // 归还缓冲区
                   }
                   System.out.println("File " + fileName + " download successful.");
                   fos.close();
               }
           } catch (IOException | InterruptedException e) {
               System.err.println("Error: " + e.getMessage());
           }
		return null;
    }
    
    @Override
    public void run() {
    	try {
			call();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
}
