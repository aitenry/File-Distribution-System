package com.aiten.fds.handles.client;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class CRFileInfo implements Runnable {

	private static final int SERVER_PORT = 7777;
	private static final String SERVER_IP = "127.0.0.1";

	private List<String> userFileInfoList = new ArrayList<>();
	private List<String> adminFileInfoList = new ArrayList<>();
	private final String username;
	
	private final CountDownLatch latch = new CountDownLatch(1);
	
	public CRFileInfo(String username) {
		this.username = username;
	}

	@Override
	public void run() {
		try {
			Socket socket = new Socket(SERVER_IP, SERVER_PORT);
			System.out.println("Connected to server " + SERVER_IP + ":" + SERVER_PORT);

			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
			
			writer.println(username);

			StringBuilder responseBuilder = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.equals("*")) {
					responseBuilder.append(line);
				} else {
					responseBuilder.append(line).append(",");
				}
			}
			
			if (!responseBuilder.toString().equals("*")) {
				String[] fileInfo = responseBuilder.toString().split("\\*");
				userFileInfoList = Arrays.asList(fileInfo[0].split(",")); // 相应用户路径下的文件
				adminFileInfoList = Arrays.asList(fileInfo[1].split(",")); // 管理员路径下的文件
			}
			

			socket.close();
			System.out.println("Disconnected from server " + SERVER_IP + ":" + SERVER_PORT);

			latch.countDown();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public List<String> getUserFileInfoList() throws InterruptedException {
		latch.await();
        return userFileInfoList;
    }
    
    public List<String> getAdminFileInfoList() throws InterruptedException {
    	latch.await();
        return adminFileInfoList;
    }
    
}
