package com.aiten.fds.handles.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import com.aiten.fds.domain.FileInfo;
import com.aiten.fds.domain.UserFile;

public class CRAllFileInfo implements Runnable {

	private static final int SERVER_PORT = 4444;
	private static final String SERVER_IP = "127.0.0.1";

	private List<UserFile> userFiles;

	private final CountDownLatch latch = new CountDownLatch(1);

	@Override
	public void run() {
		try {
			Socket socket = new Socket(SERVER_IP, SERVER_PORT);
			System.out.println("Connected to server " + SERVER_IP + ":" + SERVER_PORT);
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			userFiles = new ArrayList<>();
			StringBuilder responseBuilder = new StringBuilder();

			String line;
			while ((line = reader.readLine()) != null) {
				responseBuilder.append(line).append(",");
				if (line.equals("]")) {
					responseBuilder.append(line).append(",");
					String[] strs = responseBuilder.toString().split(" = \\[, |,|]");
					String username = strs[0].substring(0, strs[0].length() - 4);
					List<FileInfo> fileInfos = new ArrayList<>();
					for (int i = 1; i < strs.length; i += 3) {
						fileInfos.add(new FileInfo(strs[i], strs[i + 1], strs[i + 2]));
					}
					userFiles.add(new UserFile(username, fileInfos));
					responseBuilder.setLength(0);
				}
			}
			
			socket.close();
			System.out.println("Disconnected from server " + SERVER_IP + ":" + SERVER_PORT);

			latch.countDown();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public List<UserFile> getUserFileInfoList() throws InterruptedException {
		latch.await();
		return userFiles;
	}
	
}
