package com.aiten.fds.handles.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.aiten.fds.dao.UserDAO;
import com.aiten.fds.domain.User;

public class SSUserInfo {

	private static final int SERVER_PORT = 9999; // 服务器端口号

	public void startServer() {
		try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT)) {
			// 打印服务器启动信息
			System.out.println("Server started, listening on port " + SERVER_PORT + "...");

			// 当外部通过调用 Thread.interrupt() 中断线程时，循环会通过 isInterrupted() 返回 true，从而跳出循环
			while (!Thread.currentThread().isInterrupted()) {
				Socket socket = serverSocket.accept();
				// 打印新连接信息
				System.out.println("New connection accepted: " + socket.getInetAddress() + ":" + socket.getPort());

				new Thread(new ServerTask(socket)).start();
			}
		} catch (IOException e) {
			// 打印异常堆栈信息
			e.printStackTrace();
		}
	}

	private record ServerTask(Socket socket) implements Runnable {

		@Override
		public void run() {
			try {
				PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
				List<String> fileUserInfoList = new ArrayList<>();
				for (User user : new UserDAO().getUserList()) {
					fileUserInfoList.add(user.toString());
				}
				String fileInfoString = String.join("\n", fileUserInfoList);
				writer.println(fileInfoString);
				socket.close();
				System.out.println("Connection closed: " + socket.getInetAddress() + ":" + socket.getPort());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
