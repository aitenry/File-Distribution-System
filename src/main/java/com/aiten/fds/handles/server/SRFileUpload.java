package com.aiten.fds.handles.server;

import java.io.*;
import java.net.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aiten.fds.dao.AdminUploadDAO;
import com.aiten.fds.dao.UserUploadDAO;
import com.aiten.fds.domain.UploadFile;
import com.aiten.fds.utils.FileBufferUtils;

public class SRFileUpload {

	private static final int SERVER_PORT = 8888; // 服务器端口号
	private static final int USER_MAX_COUNT = 20;
	private static final int ADMIN_MAX_COUNT = 20;
	private static int userVariable = 0;
	private static int adminVariable = 0;
	private static final List<UploadFile> userUploadFileList = new ArrayList<>();
	private static final List<UploadFile> adminUploadFileList = new ArrayList<>();

	public void startServer() {
		try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT)) {
			// 打印服务器启动信息
			System.out.println("Server started, listening on port " + SERVER_PORT + "...");

			// 当外部通过调用 Thread.interrupt() 中断线程时，循环会通过 isInterrupted() 返回 true，从而跳出循环
			while (!Thread.currentThread().isInterrupted()) {
				Socket socket = serverSocket.accept();
				// 打印新连接信息
				System.out.println("New connection accepted: " + socket.getInetAddress() + ":" + socket.getPort());

				new Thread(new ServerTask(socket)).start();
			}
		} catch (IOException e) {
			// 打印异常堆栈信息
			e.printStackTrace();
		}
	}

	private static class ServerTask implements Runnable {
		private final Socket socket;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		public ServerTask(Socket socket) {
			this.socket = socket;
		}

		@Override
		public void run() {
			try (DataInputStream dataInputStream = new DataInputStream(socket.getInputStream())) {
				// 读取客户端发送的文件名和内容，并保存到本地目录
				String fileInfo = dataInputStream.readUTF();
				String username = null;
				if (fileInfo.contains("/admin/")) {
					username = fileInfo.split("\\*")[1];
					fileInfo = fileInfo.split("\\*")[0];
				}
				FileOutputStream fileOutputStream = new FileOutputStream(fileInfo);
				String fileName = fileInfo.substring(fileInfo.lastIndexOf("/") + 1);

				long fileSize = dataInputStream.readLong(); // 读取文件大小

				byte[] buffer;

				while ((buffer = FileBufferUtils.getFileBuffer(fileSize)) != null) {
					int len = dataInputStream.read(buffer);
					if (len == -1) {
						break;
					}
					fileOutputStream.write(buffer, 0, len);
					FileBufferUtils.returnFileBuffer(buffer); // 归还缓冲区
				}

				System.out.println("File " + fileName + " received.");
				fileOutputStream.close();

				try {
					if (fileInfo.contains("/users/")) {
						userUploadFileList.add(new UploadFile(
								fileInfo.split("/users/")[1].split("/")[0],
								fileName, dateFormat.parse(dateFormat.format(new Date())), fileInfo, fileSize));
					}
					else if(fileInfo.contains("/admin/")) {
						adminUploadFileList.add(new UploadFile(
								username, fileName,
								dateFormat.parse(dateFormat.format(new Date())), fileInfo, fileSize));
					}

				} catch (ParseException e1) {
					e1.printStackTrace();
				}

				// 当 userUploadFileList 的大小达到 USER_MAX_COUNT 时，提交数据到数据库
				if (++userVariable == USER_MAX_COUNT) {
					try {
						new UserUploadDAO().insertUserUploadOverall(userUploadFileList);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					userUploadFileList.clear();
					userVariable = 0;
				}
				// 当 userUploadFileList 的大小达到 USER_MAX_COUNT 时，提交数据到数据库
				if (++adminVariable == ADMIN_MAX_COUNT) {
					try {
						new AdminUploadDAO().insertAdminUploadOverall(adminUploadFileList);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					adminUploadFileList.clear();
					adminVariable = 0;
				}

				// 关闭socket
				socket.close();
			} catch (IOException | InterruptedException e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}
}
