package com.aiten.fds.handles.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import com.aiten.fds.dao.AdminDAO;
import com.aiten.fds.dao.UserDAO;

public class SSLoginVerify {
	private static final int SERVER_PORT = 5555; // 服务器端口号

	public void startServer() {
		try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT)) {
			// 打印服务器启动信息
			System.out.println("Server started, listening on port " + SERVER_PORT + "...");

			// 当外部通过调用 Thread.interrupt() 中断线程时，循环会通过 isInterrupted() 返回 true，从而跳出循环
			while (!Thread.currentThread().isInterrupted()) {
				Socket socket = serverSocket.accept();
				// 打印新连接信息
				System.out.println("New connection accepted: " + socket.getInetAddress() + ":" + socket.getPort());

				new Thread(new ServerTask(socket)).start();
			}
		} catch (IOException e) {
			// 打印异常堆栈信息
			e.printStackTrace();
		}
	}

	private record ServerTask(Socket socket) implements Runnable {

		@Override
		public void run() {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);

				String username = reader.readLine();
				String password = reader.readLine();
				String type = reader.readLine();

				boolean verify = false;
				if (type.equals("users")) {
					verify = new UserDAO().verifyUser(username, password);
					if (verify) {
						new UserDAO().updateUserOnline(username, 1);
					}
				} else if (type.equals("admin")) {
					verify = new AdminDAO().verifyAdmin(username, password);
				}
				writer.println(verify);

				socket.close();
				System.out.println("Connection closed: " + socket.getInetAddress() + ":" + socket.getPort());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
