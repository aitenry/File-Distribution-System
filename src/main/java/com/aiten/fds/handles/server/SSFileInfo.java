package com.aiten.fds.handles.server;

import java.io.*;
import java.net.*;
import java.util.*;

import com.aiten.fds.utils.DataHandleUtils;

public class SSFileInfo {
	private static final int SERVER_PORT = 7777; // 服务器端口号

	public void startServer() {
		try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT)) {
			// 打印服务器启动信息
			System.out.println("Server started, listening on port " + SERVER_PORT + "...");

			// 当外部通过调用 Thread.interrupt() 中断线程时，循环会通过 isInterrupted() 返回 true，从而跳出循环
			while (!Thread.currentThread().isInterrupted()) {
				Socket socket = serverSocket.accept();
				// 打印新连接信息
				System.out.println("New connection accepted: " + socket.getInetAddress() + ":" + socket.getPort());

				new Thread(new ServerTask(socket)).start();
			}
		} catch (IOException e) {
			// 打印异常堆栈信息
			e.printStackTrace();
		}
	}

	private record ServerTask(Socket socket) implements Runnable {

		@Override
		public void run() {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);

				String username = reader.readLine();
				File userFolder = new File("src/main/resources/warehouse/users/" + username);
				File adminFolder = new File("src/main/resources/warehouse/admin");
				if (userFolder.exists() && userFolder.isDirectory()) {
					File[] userFiles = userFolder.listFiles();
					File[] adminFiles = adminFolder.listFiles();
					List<String> fileInfoList = new ArrayList<>();
					List<String> fileUserInfoList = new ArrayList<>();
					List<String> fileAdminInfoList = new ArrayList<>();
					assert userFiles != null;
					for (File file : userFiles) {
						String fileInfo = file.getName() + "," + DataHandleUtils.convertFileSize(file.length());
						fileUserInfoList.add(fileInfo);
					}
					fileUserInfoList.add("*");
					assert adminFiles != null;
					for (File file : adminFiles) {
						String fileInfo = file.getName() + "," + DataHandleUtils.convertFileSize(file.length());
						fileAdminInfoList.add(fileInfo);
					}
					fileInfoList.addAll(fileUserInfoList);
					fileInfoList.addAll(fileAdminInfoList);
					String fileInfoString = String.join("\n", fileInfoList);
					writer.println(fileInfoString);
				} else {
					writer.println("The user directory does not exist!");
				}

				socket.close();
				System.out.println("Connection closed: " + socket.getInetAddress() + ":" + socket.getPort());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
