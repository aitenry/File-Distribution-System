package com.aiten.fds.handles.server;

import java.io.*;
import java.net.*;

import com.aiten.fds.utils.FileBufferUtils;

public class SSFileDownload {

	private static final int SERVER_PORT = 6666; // 服务器端口号

	public void startServer() {
		try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT)) {
			// 打印服务器启动信息
			System.out.println("Server started, listening on port " + SERVER_PORT + "...");

			// 当外部通过调用 Thread.interrupt() 中断线程时，循环会通过 isInterrupted() 返回 true，从而跳出循环
			while (!Thread.currentThread().isInterrupted()) {
				Socket socket = serverSocket.accept();
				// 打印新连接信息
				System.out.println("New connection accepted: " + socket.getInetAddress() + ":" + socket.getPort());

				new Thread(new ServerTask(socket)).start();
			}
		} catch (IOException e) {
			// 打印异常堆栈信息
			e.printStackTrace();
		}
	}

	private record ServerTask(Socket socket) implements Runnable {

		@Override
		public void run() {
			try {
				InputStream is = socket.getInputStream(); // 获取输入流
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String fileInfo = br.readLine(); // 读取客户端发送的文件路径

				File file = new File(fileInfo);
				DataOutputStream dout = new DataOutputStream(socket.getOutputStream());

				if (!file.exists()) { // 如果文件不存在
					dout.writeInt(-1); // 发送-1表示文件不存在
					dout.flush();
				} else { // 如果文件存在
					long fileSize = file.length();
					dout.writeInt(0); // 发送0表示文件存在
					dout.writeLong(fileSize); // 发送文件长度
					dout.flush();

					FileInputStream fis = new FileInputStream(file);
					byte[] buffer;

					while ((buffer = FileBufferUtils.getFileBuffer(fileSize)) != null) {
						int len = fis.read(buffer);
						if (len == -1) {
							break;
						}
						socket.getOutputStream().write(buffer, 0, len);
						socket.getOutputStream().flush();
						FileBufferUtils.returnFileBuffer(buffer); // 归还缓冲区
					}

					fis.close();
				}

				socket.close();
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
