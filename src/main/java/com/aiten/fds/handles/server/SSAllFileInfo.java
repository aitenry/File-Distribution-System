package com.aiten.fds.handles.server;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.aiten.fds.utils.DataHandleUtils;

public class SSAllFileInfo {
	private static final int SERVER_PORT = 4444; // 服务器端口号

	public void startServer() {
		try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT)) {
			// 打印服务器启动信息
			System.out.println("Server started, listening on port " + SERVER_PORT + "...");

			// 当外部通过调用 Thread.interrupt() 中断线程时，循环会通过 isInterrupted() 返回 true，从而跳出循环
			while (!Thread.currentThread().isInterrupted()) {
				Socket socket = serverSocket.accept();
				// 打印新连接信息
				System.out.println("New connection accepted: " + socket.getInetAddress() + ":" + socket.getPort());

				new Thread(new ServerTask(socket)).start();
			}
		} catch (IOException e) {
			// 打印异常堆栈信息
			e.printStackTrace();
		}
	}


	private record ServerTask(Socket socket) implements Runnable {

		@Override
		public void run() {
			try {
				PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				File userFolder = new File("src/main/resources/warehouse/users");
				File adminFolder = new File("src/main/resources/warehouse/admin");
				FileTime fileTime = null;
				if (userFolder.exists() && userFolder.isDirectory()) {
					List<String> fileInfoList = new ArrayList<>();
					List<String> fileUserInfoList = new ArrayList<>();
					List<String> fileAdminInfoList = new ArrayList<>();
					for (File folder : Objects.requireNonNull(userFolder.listFiles())) {
						fileUserInfoList.add(folder.getName() + " = [");
						for (File userFile : Objects.requireNonNull(folder.listFiles())) {
							try {
								fileTime = Files.readAttributes(userFile.toPath(), BasicFileAttributes.class).creationTime();
							} catch (IOException e) {
								e.printStackTrace();
							}
							assert fileTime != null;
							String fileInfo = userFile.getName() + "," +
									DataHandleUtils.convertFileSize(userFile.length()) + "," +
									dateFormat.format(fileTime.toMillis());
							fileUserInfoList.add(fileInfo);
						}
						fileUserInfoList.add("]");
					}
					fileAdminInfoList.add("公共文件 = [");
					try {
						for (File adminFile : Objects.requireNonNull(adminFolder.listFiles())) {
							try {
								fileTime = Files.readAttributes(adminFile.toPath(), BasicFileAttributes.class).creationTime();
							} catch (IOException e) {
								e.printStackTrace();
							}
							assert fileTime != null;
							String fileInfo = adminFile.getName() + "," +
									DataHandleUtils.convertFileSize(adminFile.length()) + "," +
									dateFormat.format(fileTime.toMillis());
							fileAdminInfoList.add(fileInfo);
						}
					} catch (Exception e) {
						System.out.println("No file information added...");
					} finally {
						fileAdminInfoList.add("]");
						fileInfoList.addAll(fileUserInfoList);
						fileInfoList.addAll(fileAdminInfoList);
						String fileInfoString = String.join("\n", fileInfoList);
						writer.println(fileInfoString);
					}
				} else {
					writer.println("The user directory does not exist!");
				}

				socket.close();
				System.out.println("Connection closed: " + socket.getInetAddress() + ":" + socket.getPort());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
