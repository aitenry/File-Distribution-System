package com.aiten.fds;

import com.aiten.fds.ui.LoginPage;
import javafx.application.Application;

public class FDSLaunch {
	public static void main(String[] args) {
		Application.launch(LoginPage.class, args);
	}
}
