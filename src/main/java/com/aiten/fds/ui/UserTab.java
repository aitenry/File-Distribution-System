package com.aiten.fds.ui;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import com.aiten.fds.handles.client.CRFileDownload;
import com.aiten.fds.handles.client.CSFileUpload;
import com.aiten.fds.handles.client.CRFileInfo;
import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

public class UserTab {

	private static List<File> selectedFiles;

	public static Tab createUserUploadTab(Stage primaryStage, String username) {
		Tab uploadTab = new Tab();
		VBox uploadOverall = new VBox();
		uploadOverall.setSpacing(60);
		uploadOverall.setTranslateY(-60);

		Separator uploadSeparator = new Separator();
		uploadSeparator.setTranslateY(30);

		uploadTab.setText("上传文件");
		uploadTab.setClosable(false);

		Button uploadBtn = new Button("上传文件");
		uploadBtn.setPrefWidth(260);
		uploadBtn.setPrefHeight(66);

		uploadOverall.setAlignment(Pos.CENTER);

		VBox infoVBox = new VBox();
		infoVBox.setAlignment(Pos.CENTER);
		infoVBox.setMinWidth(300);
		infoVBox.setMinHeight(120);
		infoVBox.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: dashed inside;"
				+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

		GridPane uploadFilesGrid = new GridPane();
		uploadFilesGrid.setAlignment(Pos.CENTER);
		uploadFilesGrid.setPadding(new Insets(10));
		uploadFilesGrid.setHgap(20);
		uploadFilesGrid.setVgap(20);
		uploadFilesGrid.setMaxWidth(930);
		uploadFilesGrid.setMinHeight(440);
		uploadFilesGrid.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
				+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

		ScrollPane uploadScrollPane = new ScrollPane(uploadFilesGrid);
		uploadScrollPane.setPadding(new Insets(10));
		uploadScrollPane.setFitToWidth(true);
		uploadScrollPane.setMaxWidth(940);
		uploadScrollPane.setMinHeight(466);
		uploadScrollPane.setMaxHeight(466);
		uploadScrollPane.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
				+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

		ImageView imageView = new ImageView(new Image(
				Objects.requireNonNull(LoginPage.class.getResource("/img/uploadFile.png")).toExternalForm()));
		Label uploadLabel = new Label("点击选择需要上传的文件");

		uploadFilesGrid.setOnMouseClicked(event -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("选择文件");
			selectedFiles = fileChooser.showOpenMultipleDialog(primaryStage);
			if (selectedFiles != null && !selectedFiles.isEmpty()) {
				if (selectedFiles.size() > 20) {
					System.out.println("The number of uploaded files exceeds the limit.");
				} else {
					uploadFilesGrid.getChildren().clear();
					int rowIndex = 0;
					int colIndex = 0;
					for (File file : selectedFiles) {
						HBox uploadFileOverall = new HBox();
						uploadFileOverall.setPadding(new Insets(10));
						uploadFileOverall.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
								+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

						VBox uploadFileInfo = new VBox();
						uploadFileInfo.setSpacing(7);
						uploadFileInfo.setMaxWidth(370);
						uploadFileInfo.setMaxHeight(53);

						Label uploadFileLabel = new Label(file.getName());
						uploadFileLabel.setFont(new Font("Yu Gothic Medium", 16));
						uploadFileLabel.setMaxWidth(300);

						ImageView fileImageView = new ImageView(new Image(
								Objects.requireNonNull(LoginPage.class.getResource("/img/uploadFile.png")).toExternalForm()));

						ProgressBar uploadFileBar = new ProgressBar(0);
						uploadFileBar.setMinWidth(300);

						uploadFileInfo.getChildren().addAll(uploadFileLabel, uploadFileBar);
						uploadFileOverall.getChildren().addAll(fileImageView, uploadFileInfo);

						uploadFileOverall.setAlignment(Pos.BOTTOM_CENTER);
						uploadFileInfo.setAlignment(Pos.CENTER_LEFT);
						uploadFileOverall.setSpacing(7);

						uploadFilesGrid.add(uploadFileOverall, colIndex, rowIndex);

						colIndex++;
						if (colIndex > 1) {
							colIndex = 0;
							rowIndex++;
						}
					}
				}
			}
		});

		uploadBtn.setOnAction(event -> {
			if (selectedFiles != null) {
				LinkedList<ProgressBar> barList = new LinkedList<>();
				for (Node node : uploadFilesGrid.getChildren()) {
					if (node instanceof HBox hbox) {
						for (Node childNode : hbox.getChildren()) {
							if (childNode instanceof VBox vbox) {
								for (Node innerChildNode : vbox.getChildren()) {
									if (innerChildNode instanceof ProgressBar progressBar) {
										barList.add(progressBar);
									}
								}
							}
						}
					}
				}
				int index = 0;
				for (File file : selectedFiles) {
					CSFileUpload clientSend = new CSFileUpload(file.getPath(), username, "users", barList.get(index));
					new Thread(clientSend).start();
					index++;
				}
			}

		});

		infoVBox.getChildren().addAll(imageView, uploadLabel);
		uploadFilesGrid.getChildren().add(infoVBox);

		uploadOverall.getChildren().addAll(uploadSeparator, uploadScrollPane, uploadBtn);


		uploadTab.setContent(uploadOverall);
		return uploadTab;
	}

	public static Tab createUserDownloadTab(String username) throws InterruptedException {
		CRFileInfo clientDisplay = new CRFileInfo(username);

		Thread infoThread = new Thread(clientDisplay);
		infoThread.start();

		List<String> fileUserInfoList = clientDisplay.getUserFileInfoList();
		List<String> fileAdminInfoList = clientDisplay.getAdminFileInfoList();

		Tab downloadTab = new Tab();
		downloadTab.setText("下载文件");
		downloadTab.setClosable(false);

		VBox downloadOverall = new VBox();
		downloadOverall.setTranslateY(-20);

		List<String> selectFile = Arrays.asList("我的文件", "其他文件");
		ObservableList<String> obList = FXCollections.observableArrayList(selectFile);

		ComboBox<String> comboBox = new ComboBox<>(obList);
		comboBox.setPrefWidth(1270);
		comboBox.setPrefHeight(30);
		comboBox.setTranslateX(-8.7);
		comboBox.setTranslateY(-27);
		comboBox.getSelectionModel().select(0);

		Separator downloadSeparator = new Separator();
		downloadSeparator.setTranslateY(-30);

		GridPane downloadUserFilesGrid = new GridPane();
		downloadUserFilesGrid.setAlignment(Pos.CENTER);
		downloadUserFilesGrid.setPadding(new Insets(7));
		downloadUserFilesGrid.setHgap(20);
		downloadUserFilesGrid.setVgap(20);
		downloadUserFilesGrid.setMaxWidth(1100);
		downloadUserFilesGrid.setMinHeight(560);
		downloadUserFilesGrid.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
				+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");
		setDownloadFilesGrid(downloadUserFilesGrid, fileUserInfoList, username, "users");

		GridPane downloadOrderFilesGrid = new GridPane();
		downloadOrderFilesGrid.setAlignment(Pos.CENTER);
		downloadUserFilesGrid.setPadding(new Insets(7));
		downloadOrderFilesGrid.setHgap(20);
		downloadOrderFilesGrid.setVgap(20);
		downloadOrderFilesGrid.setMaxWidth(1100);
		downloadOrderFilesGrid.setMinHeight(560);
		downloadOrderFilesGrid.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
				+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");
		setDownloadFilesGrid(downloadOrderFilesGrid, fileAdminInfoList, username, "admin");

		ScrollPane downloadUserScrollPane = new ScrollPane(downloadUserFilesGrid);
		downloadUserScrollPane.setPadding(new Insets(10));
		downloadUserScrollPane.setFitToWidth(true);
		downloadUserScrollPane.setMaxWidth(1100);
		downloadUserScrollPane.setMaxHeight(580);
		downloadUserScrollPane.setMinWidth(1100);
		downloadUserScrollPane.setMinHeight(580);

		ScrollPane downloadOrderScrollPane = new ScrollPane(downloadOrderFilesGrid);
		downloadOrderScrollPane.setPadding(new Insets(10));
		downloadOrderScrollPane.setFitToWidth(true);
		downloadOrderScrollPane.setMaxWidth(1100);
		downloadOrderScrollPane.setMaxHeight(580);
		downloadOrderScrollPane.setMinWidth(1100);
		downloadOrderScrollPane.setMinHeight(580);


		TranslateTransition ttUserFileScrollPane = new TranslateTransition(Duration.seconds(0.5), downloadUserScrollPane);
		ttUserFileScrollPane.setInterpolator(Interpolator.EASE_OUT);

		TranslateTransition ttOrderFileScrollPane = new TranslateTransition(Duration.seconds(0.5), downloadOrderScrollPane);
		ttOrderFileScrollPane.setInterpolator(Interpolator.EASE_OUT);

		comboBox.setOnShown(event -> {
			ttUserFileScrollPane.setFromY(0);
			ttUserFileScrollPane.setToY(30);
			ttUserFileScrollPane.play();
			ttOrderFileScrollPane.setFromY(0);
			ttOrderFileScrollPane.setToY(30);
			ttOrderFileScrollPane.play();
		});

		comboBox.setOnHidden(event -> {
			ttUserFileScrollPane.setFromY(30);
			ttUserFileScrollPane.setToY(0);
			ttUserFileScrollPane.play();
			ttOrderFileScrollPane.setFromY(30);
			ttOrderFileScrollPane.setToY(0);
			ttOrderFileScrollPane.play();
		});

		comboBox.setButtonCell(new ListCell<>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setText(null);
				} else {
					setText(item);
					setAlignment(Pos.CENTER); // 设置文本居中对齐
					setTranslateX(16);
				}
			}
		});

		comboBox.setCellFactory(param -> new ListCell<>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setText(null);
				} else {
					setText(item);
					setAlignment(Pos.CENTER); // 设置文本居中对齐
				}
			}
		});

		comboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.equals("我的文件")) {
				downloadOverall.getChildren().remove(2);
				downloadOverall.getChildren().add(2, downloadUserScrollPane);
			} else if (newValue.equals("其他文件")) {
				downloadOverall.getChildren().remove(2);
				downloadOverall.getChildren().add(2, downloadOrderScrollPane);
			}
		});

		downloadOverall.setAlignment(Pos.CENTER);
		downloadOverall.getChildren().addAll(downloadSeparator, comboBox, downloadUserScrollPane);
		downloadTab.setContent(downloadOverall);

		return downloadTab;
	}

	private static void setDownloadFilesGrid(GridPane downloadTypeFilesGrid, List<String> fileInfoList, String username, String type) {
		int colIndex = 0;
		int rowIndex = 0;
		for (int i = 0; i < fileInfoList.size() && fileInfoList.size() != 1; i = i + 2) {
			String fileName = fileInfoList.get(i);

			HBox fileOverall = new HBox();

			HBox hboxC = new HBox();
			hboxC.setSpacing(3);

			fileOverall.setPadding(new Insets(7.7));
			fileOverall.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
					+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

			VBox fileInfo = new VBox();
			fileInfo.setSpacing(7);
			fileInfo.setMaxWidth(370);
			fileInfo.setMaxHeight(73);

			Label fileNameLabel = new Label(fileName);
			fileNameLabel.setFont(new Font("Yu Gothic Medium", 12));
			fileNameLabel.setMaxWidth(200);

			Tooltip.install(fileNameLabel, new Tooltip(fileName));

			Label fileSizeLabel = new Label("文件大小：" + fileInfoList.get(i + 1));
			fileSizeLabel.setFont(new Font("Yu Gothic Medium", 12));
			fileSizeLabel.setMaxWidth(200);

			ProgressBar downloadFileBar = new ProgressBar(0);
			downloadFileBar.setMinWidth(200);

			ImageView fileImageView = new ImageView(new Image(
					Objects.requireNonNull(LoginPage.class.getResource("/img/downloadFile.png")).toExternalForm()));

			Button btnDownload = new Button("下载文件");

			btnDownload.setOnAction(event -> {
				CRFileDownload crFileDownload = new CRFileDownload(fileName, username, type, downloadFileBar);
				new Thread(crFileDownload).start();
			});

			hboxC.getChildren().addAll(btnDownload, fileSizeLabel);
			fileInfo.getChildren().addAll(fileNameLabel, hboxC, downloadFileBar);
			fileOverall.getChildren().addAll(fileImageView, fileInfo);

			fileOverall.setAlignment(Pos.BOTTOM_CENTER);
			fileInfo.setAlignment(Pos.CENTER_LEFT);
			hboxC.setAlignment(Pos.CENTER_LEFT);
			fileOverall.setSpacing(7);

			downloadTypeFilesGrid.add(fileOverall, colIndex, rowIndex);

			colIndex++;
			if (colIndex > 2) {
				colIndex = 0;
				rowIndex++;
			}
		}
	}

}
