package com.aiten.fds.ui;

import com.aiten.fds.handles.client.CSCmdProcess;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Objects;

public class OverallPage{

	public Stage createAdminPage(String username) {
		Stage primaryStage = new Stage();

		AnchorPane adminAP = new AnchorPane();

		TabPane adminTP = new TabPane();
		adminTP.setTabMinWidth(413);
		adminTP.setTabMinHeight(39);

		try {
			adminTP.getTabs().addAll(
					AdminTab.createUserInfoTab(primaryStage),
					AdminTab.createFileManageTab(),
					AdminTab.createAdminUploadTab(primaryStage, username));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Button btnRefresh = new Button("刷新", new ImageView(new Image(
				Objects.requireNonNull(LoginPage.class.getResource("/img/refresh.png")).toExternalForm())));
		btnRefresh.setMinWidth(1239);
		btnRefresh.setTranslateX(-9);
		Tab userInfoTab = adminTP.getTabs().get(0);
		VBox userInfoOverall = (VBox) userInfoTab.getContent();
		userInfoOverall.getChildren().add(1, btnRefresh);
		btnRefresh.setOnAction(e -> {
			adminTP.getTabs().clear();
			try {
				adminTP.getTabs().addAll(
						AdminTab.createUserInfoTab(primaryStage),
						AdminTab.createFileManageTab(),
						AdminTab.createAdminUploadTab(primaryStage, username));
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			Button newBtnRefresh =  new Button("刷新", new ImageView(new Image(
					Objects.requireNonNull(LoginPage.class.getResource("/img/refresh.png")).toExternalForm())));
			newBtnRefresh.setMinWidth(1239);
			newBtnRefresh.setTranslateX(-9);
			Tab newUserInfoTab = adminTP.getTabs().get(0);
			VBox newUserInfoOverall = (VBox) newUserInfoTab.getContent();
			newUserInfoOverall.getChildren().add(1, btnRefresh);
		});

		adminAP.getChildren().add(adminTP);
		Scene userScene = new Scene(adminAP);

		primaryStage.setTitle("管理员界面");
		primaryStage.getIcons().add(new Image(
				Objects.requireNonNull(LoginPage.class.getResource("/img/logo.png")).toExternalForm()));
		primaryStage.setResizable(false);
		primaryStage.setWidth(1300);
		primaryStage.setHeight(770);
		primaryStage.setScene(userScene);
		primaryStage.show();
		return primaryStage;
	}

	public Stage createUserPage(String username, String password) {
		Stage primaryStage = new Stage();

		AnchorPane userAP = new AnchorPane();

		TabPane userTP = new TabPane();
		userTP.setTabMinWidth(625);
		userTP.setTabMinHeight(39);

		userTP.setMinWidth(1250);
		userTP.setMinHeight(770);

		try {
			userTP.getTabs().addAll(
					UserTab.createUserUploadTab(primaryStage, username),
					UserTab.createUserDownloadTab(username));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		userAP.getChildren().addAll(userTP);
		Scene userScene = new Scene(userAP);

		primaryStage.setOnCloseRequest(event -> new Thread(new CSCmdProcess(username, password, "user offline")).start());

		primaryStage.setTitle("用户界面");
		primaryStage.getIcons().add(new Image(
				Objects.requireNonNull(LoginPage.class.getResource("/img/logo.png")).toExternalForm()));
		primaryStage.setResizable(false);
		primaryStage.setWidth(1300);
		primaryStage.setHeight(770);
		primaryStage.setScene(userScene);
		primaryStage.show();
		return primaryStage;
	}

}
