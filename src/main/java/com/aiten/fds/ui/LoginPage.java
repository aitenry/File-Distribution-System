package com.aiten.fds.ui;

import com.aiten.fds.handles.client.CRLoginVerify;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Separator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Objects;

public class LoginPage extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
		AnchorPane loginAP = new AnchorPane();

		TabPane loginTP = new TabPane();

		loginTP.setTabMinWidth(182);
		loginTP.setTabMinHeight(30);
		loginTP.setMaxWidth(600);

		Tab userTab = new Tab();
		Tab adminTab = new Tab();
		Tab aboutTab = new Tab();

		userTab.setText("用户登录");
		userTab.setClosable(false);

		adminTab.setText("管理员登录");
		adminTab.setClosable(false);

		aboutTab.setText("系统操作指南");
		aboutTab.setClosable(false);

		HBox hboxError = new HBox();
		hboxError.setAlignment(Pos.CENTER);
		ImageView imageViewError = new ImageView(new Image(
				Objects.requireNonNull(LoginPage.class.getResource("/img/dropR.png")).toExternalForm()));
		hboxError.getChildren().addAll(imageViewError, new Text());

		//用户登录窗口样式
		VBox userVBox = new VBox();
		VBox userVboxF = new VBox();
		VBox userVboxS = new VBox();

		userVBox.setPrefWidth(600);
		userVBox.setPrefHeight(360);

		Separator userSeparator = new Separator();
		userSeparator.setTranslateY(-27);

		Text userTextF = new Text("用户名");
		userTextF.setFont(new Font(17));
		userTextF.setTranslateX(121);

		TextField username = new TextField();
		username.setMaxWidth(300);
		username.setPrefHeight(36);
		username.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"
		);
		username.setOnMouseEntered(e -> username.setStyle(
				"-fx-background-color: linear-gradient(to right, #7B68EE30, #6699CC30);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));
		username.setOnMouseExited(e -> username.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));

		Text userTextS = new Text("密码");
		userTextS.setFont(new Font(17));
		userTextS.setTranslateX(129);

		PasswordField userPassword = new PasswordField();
		userPassword.setMaxWidth(300);
		userPassword.setPrefHeight(36);
		userPassword.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"
		);
		userPassword.setOnMouseEntered(e -> userPassword.setStyle(
				"-fx-background-color: linear-gradient(to right, #7B68EE30, #6699CC30);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));
		userPassword.setOnMouseExited(e -> userPassword.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));

		Button userBtn = new Button("登录");
		userBtn.setPrefWidth(160);
		userBtn.setPrefHeight(36);
		userBtn.setTranslateX(216);
		userBtn.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996699, #FFCC9999);"
						+ "-fx-background-radius: 25px;"
						+ "-fx-border-radius: 25px;"
		);
		userBtn.setOnMouseEntered(e -> userBtn.setStyle(
				"-fx-background-color: linear-gradient(to right, #7B68EE99, #6699CC99);"
						+ "-fx-background-radius: 25px;"
						+ "-fx-border-radius: 25px;"));
		userBtn.setOnMouseExited(e -> userBtn.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996699, #FFCC9999);"
						+ "-fx-background-radius: 25px;"
						+ "-fx-border-radius: 25px;"));

		userBtn.setOnAction(e -> {
			String usernameStr = username.getText();
			String passwordStr = userPassword.getText();
			if (!usernameStr.equals("") && !passwordStr.equals("")) {
				CRLoginVerify crLoginVerify = new CRLoginVerify(usernameStr, passwordStr, "users");
				new Thread(crLoginVerify).start();
				try {
					if (crLoginVerify.getVerifyAns()) {
						primaryStage.close();
						new OverallPage().createUserPage(usernameStr, passwordStr).show();
					} else {
						Text text = (Text) hboxError.getChildren().get(1);
						text.setText("输入的用户名或者密码错误，请重新输入！");
						if(userVBox.getChildren().indexOf(hboxError) != 4) {
							userVBox.getChildren().add(hboxError);
						}
					}
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			} else {
				Text text = (Text) hboxError.getChildren().get(1);
				text.setText("请输入用户名或者密码，用户名或密码不能为空！");
				if(userVBox.getChildren().indexOf(hboxError) != 4) {
					userVBox.getChildren().add(hboxError);
				}
			}
		});

		userVBox.setSpacing(20);
		userVBox.setTranslateY(30);

		userVboxF.setTranslateX(150);
		userVboxF.setSpacing(10);

		userVboxS.setTranslateX(150);
		userVboxS.setSpacing(10);



		//管理员登录窗口样式
		VBox adminVBox = new VBox();
		VBox adminVboxF = new VBox();
		VBox adminVboxS = new VBox();

		adminVBox.setPrefWidth(600);
		adminVBox.setPrefHeight(300);

		Separator adminSeparator = new Separator();
		adminSeparator.setTranslateY(-27);

		Text adminTextF = new Text("账号");
		adminTextF.setFont(new Font(17));
		adminTextF.setTranslateX(129);

		TextField admin_name = new TextField();
		admin_name.setMaxWidth(300);
		admin_name.setPrefHeight(36);
		admin_name.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"
		);
		admin_name.setOnMouseEntered(e -> admin_name.setStyle(
				"-fx-background-color: linear-gradient(to right, #7B68EE30, #6699CC30);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));
		admin_name.setOnMouseExited(e -> admin_name.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));

		Text adminTextS = new Text("密码");
		adminTextS.setFont(new Font(17));
		adminTextS.setTranslateX(129);

		PasswordField adminPassword = new PasswordField();
		adminPassword.setMaxWidth(300);
		adminPassword.setPrefHeight(36);
		adminPassword.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"
		);
		adminPassword.setOnMouseEntered(e -> adminPassword.setStyle(
				"-fx-background-color: linear-gradient(to right, #7B68EE30, #6699CC30);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));
		adminPassword.setOnMouseExited(e -> adminPassword.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));

		adminPassword.getText();

		Button adminBtn = new Button("登录");
		adminBtn.setPrefWidth(160);
		adminBtn.setPrefHeight(36);
		adminBtn.setTranslateX(216);
		adminBtn.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996699, #FFCC9999);"
						+ "-fx-background-radius: 25px;"
						+ "-fx-border-radius: 25px;"
		);
		adminBtn.setOnMouseEntered(e -> adminBtn.setStyle(
				"-fx-background-color: linear-gradient(to right, #7B68EE99, #6699CC99);"
						+ "-fx-background-radius: 25px;"
						+ "-fx-border-radius: 25px;"));
		adminBtn.setOnMouseExited(e -> adminBtn.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996699, #FFCC9999);"
						+ "-fx-background-radius: 25px;"
						+ "-fx-border-radius: 25px;"));

		adminBtn.setOnAction(e -> {
			String adminNameStr = admin_name.getText();
			String adminPasswordStr = adminPassword.getText();
			if (!adminNameStr.equals("") && !adminPasswordStr.equals("")) {
				CRLoginVerify crLoginVerify = new CRLoginVerify(adminNameStr, adminPasswordStr, "admin");
				new Thread(crLoginVerify).start();
				try {
					if (crLoginVerify.getVerifyAns()) {
						primaryStage.close();
						new OverallPage().createAdminPage(adminNameStr).show();
					} else {
						Text text = (Text) hboxError.getChildren().get(1);
						text.setText("输入的账户或者密码错误，请重新输入！");
						if(adminVBox.getChildren().indexOf(hboxError) != 4) {
							adminVBox.getChildren().add(hboxError);
						}
					}
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			} else {
				Text text = (Text) hboxError.getChildren().get(1);
				text.setText("请输入账户或者密码，账户或密码不能为空！");
				if(adminVBox.getChildren().indexOf(hboxError) != 4) {
					adminVBox.getChildren().add(hboxError);
				}
			}
		});

		adminVBox.setSpacing(20);
		adminVBox.setTranslateY(30);

		adminVboxF.setTranslateX(150);
		adminVboxF.setSpacing(10);

		adminVboxS.setTranslateX(150);
		adminVboxS.setSpacing(10);

		//用户登录窗口组件添加
		userVboxF.getChildren().addAll(userTextF, username);
		userVboxS.getChildren().addAll(userTextS, userPassword);
		userVBox.getChildren().addAll(userSeparator, userVboxF, userVboxS, userBtn);

		//管理员登录窗口组件添加
		adminVboxF.getChildren().addAll(adminTextF, admin_name);
		adminVboxS.getChildren().addAll(adminTextS, adminPassword);
		adminVBox.getChildren().addAll(adminSeparator, adminVboxF, adminVboxS, adminBtn);

		userTab.setContent(userVBox);
		adminTab.setContent(adminVBox);

		loginTP.getTabs().addAll(userTab, adminTab, aboutTab);
		loginAP.getChildren().add(loginTP);

		Scene loginScene = new Scene(loginAP);

		primaryStage.setTitle("登录界面");
		primaryStage.getIcons().add(new Image(
				Objects.requireNonNull(LoginPage.class.getResource("/img/logo.png")).toExternalForm()));
		primaryStage.setResizable(false);
		primaryStage.setWidth(607);
		primaryStage.setHeight(400);
		primaryStage.setScene(loginScene);
		primaryStage.show();
	}

}