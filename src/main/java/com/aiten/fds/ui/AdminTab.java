package com.aiten.fds.ui;

import java.io.File;
import java.util.*;

import com.aiten.fds.domain.FileInfo;
import com.aiten.fds.domain.UserFile;
import com.aiten.fds.handles.client.CRAllFileInfo;
import com.aiten.fds.handles.client.CRFileDownload;
import com.aiten.fds.handles.client.CRUserInfo;
import com.aiten.fds.handles.client.CSCmdProcess;
import com.aiten.fds.handles.client.CSFileUpload;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AdminTab {

	private static List<File> selectedFiles;

	public static Tab createUserInfoTab(Stage primaryStage) {
		Tab userInfoTab = new Tab();

		userInfoTab.setText("用户管理");
		userInfoTab.setClosable(false);

		VBox userInfoOverall = new VBox();
		userInfoOverall.setSpacing(7);
		userInfoOverall.setTranslateY(-20);

		Separator userInfoSeparator = new Separator();
		userInfoSeparator.setTranslateY(3);

		GridPane userInfoGrid = new GridPane();
		userInfoGrid.setAlignment(Pos.CENTER);
		userInfoGrid.setPadding(new Insets(10));
		userInfoGrid.setHgap(20);
		userInfoGrid.setVgap(20);
		userInfoGrid.setMaxWidth(1230);
		userInfoGrid.setMinHeight(470);
		userInfoGrid.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
				+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

		ScrollPane userInfoScrollPane = new ScrollPane(userInfoGrid);
		userInfoScrollPane.setPadding(new Insets(10));
		userInfoScrollPane.setFitToWidth(true);
		userInfoScrollPane.setMaxWidth(1240);
		userInfoScrollPane.setMinHeight(500);
		userInfoScrollPane.setMaxHeight(500);
		userInfoScrollPane.setTranslateX(-8);
		userInfoScrollPane.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
				+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

		Button addUserBtn = new Button("添加用户", new ImageView(new Image(
				Objects.requireNonNull(LoginPage.class.getResource("/img/addUser.png")).toExternalForm())));
		addUserBtn.setMinWidth(1239);
		addUserBtn.setTranslateX(-9);
		addUserBtn.setOnAction(e -> {
			Stage addUserStage = createAddUser(primaryStage);
			addUserStage.show();
		});

		CRUserInfo crUserInfo = new CRUserInfo();
		new Thread(crUserInfo).start();

		try {
			List<String> userInfoList = crUserInfo.getUserFileInfoList();
			int colIndex = 0;
			int rowIndex = 0;
			for (int i = 0; i < userInfoList.size() && userInfoList.size() != 1; i = i + 3) {
				String username = userInfoList.get(i);
				String password = userInfoList.get(i + 1);

				HBox userOverall = new HBox();
				userOverall.setPrefWidth(230);
				userOverall.setPrefHeight(100);
				userOverall.setPadding(new Insets(7.7));
				userOverall.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
						+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");
				userOverall.setSpacing(3);

				VBox userInfo = new VBox();
				userInfo.setSpacing(3);

				VBox imageOnlineVBox = new VBox();

				ImageView userImage = new ImageView(new Image(
						Objects.requireNonNull(LoginPage.class.getResource("/img/userInfo.png")).toExternalForm()));
				userImage.setFitWidth(64);
				userImage.setFitHeight(64);
				String tip = "用户名：" + username + "\n" + "密码：" + password;
				Tooltip.install(userImage, new Tooltip(tip));

				Label usernameLabel = new Label(username);
				usernameLabel.setFont(new Font("Yu Gothic Medium", 13));
				usernameLabel.setMaxWidth(100);
				usernameLabel.setMinWidth(100);

				Button btnDelete = new Button("删除");
				btnDelete.setMinWidth(60);

				ImageView online;
				if (userInfoList.get(i + 2).equals("1")) {
					online = new ImageView(new Image(
							Objects.requireNonNull(LoginPage.class.getResource("/img/dropG.png")).toExternalForm()));
				} else {
					online = new ImageView(new Image(
							Objects.requireNonNull(LoginPage.class.getResource("/img/dropR.png")).toExternalForm()));
				}

				userInfo.setAlignment(Pos.CENTER_LEFT);
				userOverall.setAlignment(Pos.CENTER);
				imageOnlineVBox.setAlignment(Pos.TOP_RIGHT);

				imageOnlineVBox.getChildren().add(online);
				userInfo.getChildren().addAll(usernameLabel, btnDelete);
				userOverall.getChildren().addAll(userImage, userInfo, imageOnlineVBox);
				userInfoGrid.add(userOverall, colIndex, rowIndex);

				btnDelete.setOnAction(e -> {
					CSCmdProcess cmdProcess = new CSCmdProcess(username, password, "delete user");
					new Thread(cmdProcess).start();
					try {
						if (cmdProcess.getCmdState()) {
							userInfoGrid.getChildren().remove(userOverall);
						}
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				});

				colIndex++;
				if (colIndex > 3) {
					colIndex = 0;
					rowIndex++;
				}

			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		userInfoOverall.setAlignment(Pos.CENTER);
		userInfoOverall.getChildren().addAll(userInfoSeparator, addUserBtn, userInfoScrollPane);
		userInfoTab.setContent(userInfoOverall);

		return userInfoTab;
	}

	public static Tab createFileManageTab() throws InterruptedException {
		CRAllFileInfo crAllFileInfo = new CRAllFileInfo();

		Thread infoThread = new Thread(crAllFileInfo);
		infoThread.start();

		List<String> selectFile = new ArrayList<>();

		HashMap<String, List<FileInfo>> mapFileInfo = new HashMap<>();
		HashMap<String, ScrollPane> mapFileUI = new HashMap<>();

		List<UserFile> userFileList = crAllFileInfo.getUserFileInfoList();
		for (UserFile userFile : userFileList) {
			String otUsername = userFile.getUsername();
			List<FileInfo> fileInfos = userFile.getUserFileList();
			List<String> fileInfoList = new ArrayList<>();

			selectFile.add(otUsername);
			mapFileInfo.put(otUsername, userFile.getUserFileList());
			for (FileInfo fileInfo : fileInfos) {
				fileInfoList.add(fileInfo.getFileName());
				fileInfoList.add(fileInfo.getFileSize());
				fileInfoList.add(fileInfo.getCreationTime());
			}

			GridPane filesGrid = new GridPane();
			filesGrid.setAlignment(Pos.CENTER);
			filesGrid.setPadding(new Insets(7));
			filesGrid.setHgap(20);
			filesGrid.setVgap(32);
			filesGrid.setMaxWidth(1100);
			filesGrid.setMinHeight(560);
			filesGrid.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
					+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");
			if (otUsername.equals("公共文件")) {
				setFilesInfoGrid(filesGrid, fileInfoList, otUsername, "admin");
			} else {
				setFilesInfoGrid(filesGrid, fileInfoList, otUsername, "users");
			}

			ScrollPane fileScrollPane = new ScrollPane(filesGrid);
			fileScrollPane.setPadding(new Insets(10));
			fileScrollPane.setFitToWidth(true);
			fileScrollPane.setMaxWidth(1100);
			fileScrollPane.setMaxHeight(580);
			fileScrollPane.setMinWidth(1100);
			fileScrollPane.setMinHeight(580);

			mapFileUI.put(otUsername, fileScrollPane);

		}

		Tab fileManageTab = new Tab();
		fileManageTab.setText("文件查看");
		fileManageTab.setClosable(false);

		VBox fileManageOverall = new VBox();
		fileManageOverall.setTranslateY(11);

		ObservableList<String> obList = FXCollections.observableArrayList(selectFile);

		ComboBox<String> comboBox = new ComboBox<>(obList);
		comboBox.setPrefWidth(1270);
		comboBox.setPrefHeight(30);
		comboBox.setTranslateX(-8.7);
		comboBox.setTranslateY(-17);
		comboBox.getSelectionModel().select(selectFile.indexOf("公共文件"));
		comboBox.setVisibleRowCount(3);

		Separator fileManageSeparator = new Separator();
		fileManageSeparator.setTranslateY(-23);

		comboBox.setButtonCell(new ListCell<>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setText(null);
				} else {
					setText(item);
					setAlignment(Pos.CENTER); // 设置文本居中对齐
					setTranslateX(15);
				}
			}
		});

		comboBox.setCellFactory(param -> new ListCell<>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setText(null);
				} else {
					setText(item);
					setAlignment(Pos.CENTER); // 设置文本居中对齐
					setTranslateX(6);
				}
			}
		});

		comboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.equals(oldValue)) {
				fileManageOverall.getChildren().remove(2);
				fileManageOverall.getChildren().add(2, mapFileUI.get(newValue));
			}
		});

		fileManageOverall.setAlignment(Pos.CENTER);
		fileManageOverall.getChildren().addAll(fileManageSeparator, comboBox, mapFileUI.get("公共文件"));
		fileManageTab.setContent(fileManageOverall);

		return fileManageTab;
	}

	public static Tab createAdminUploadTab(Stage primaryStage, String username) {
		Tab uploadTab = new Tab();
		VBox uploadOverall = new VBox();
		uploadOverall.setSpacing(60);
		uploadOverall.setTranslateY(-28);

		Separator uploadSeparator = new Separator();
		uploadSeparator.setTranslateY(37);

		uploadTab.setText("上传文件");
		uploadTab.setClosable(false);

		Button uploadBtn = new Button("上传文件");
		uploadBtn.setPrefWidth(260);
		uploadBtn.setPrefHeight(66);

		uploadOverall.setAlignment(Pos.CENTER);

		VBox infoVBox = new VBox();
		infoVBox.setAlignment(Pos.CENTER);
		infoVBox.setMinWidth(300);
		infoVBox.setMinHeight(120);
		infoVBox.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: dashed inside;"
				+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

		GridPane uploadFilesGrid = new GridPane();
		uploadFilesGrid.setAlignment(Pos.CENTER);
		uploadFilesGrid.setPadding(new Insets(10));
		uploadFilesGrid.setHgap(20);
		uploadFilesGrid.setVgap(20);
		uploadFilesGrid.setMaxWidth(930);
		uploadFilesGrid.setMinHeight(440);
		uploadFilesGrid.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
				+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

		ScrollPane uploadScrollPane = new ScrollPane(uploadFilesGrid);
		uploadScrollPane.setPadding(new Insets(10));
		uploadScrollPane.setFitToWidth(true);
		uploadScrollPane.setMaxWidth(940);
		uploadScrollPane.setMinHeight(466);
		uploadScrollPane.setMaxHeight(466);
		uploadScrollPane.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
				+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

		ImageView imageView = new ImageView(new Image(
				Objects.requireNonNull(LoginPage.class.getResource("/img/uploadFile.png")).toExternalForm()));
		Label uploadLabel = new Label("点击选择需要上传的文件");

		uploadFilesGrid.setOnMouseClicked(event -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("选择文件");
			selectedFiles = fileChooser.showOpenMultipleDialog(primaryStage);
			if (selectedFiles != null && !selectedFiles.isEmpty()) {
				if (selectedFiles.size() > 20) {
					System.out.println("The number of uploaded files exceeds the limit.");
				} else {
					uploadFilesGrid.getChildren().clear();
					int rowIndex = 0;
					int colIndex = 0;
					for (File file : selectedFiles) {
						HBox uploadFileOverall = new HBox();
						uploadFileOverall.setPadding(new Insets(10));
						uploadFileOverall.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
								+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

						VBox uploadFileInfo = new VBox();
						uploadFileInfo.setSpacing(7);
						uploadFileInfo.setMaxWidth(370);
						uploadFileInfo.setMaxHeight(53);

						Label uploadFileLabel = new Label(file.getName());
						uploadFileLabel.setFont(new Font("Yu Gothic Medium", 16));
						uploadFileLabel.setMaxWidth(300);

						ImageView fileImageView = new ImageView(new Image(
								Objects.requireNonNull(LoginPage.class.getResource("/img/uploadFile.png")).toExternalForm()));

						ProgressBar uploadFileBar = new ProgressBar(0);
						uploadFileBar.setMinWidth(300);

						uploadFileInfo.getChildren().addAll(uploadFileLabel, uploadFileBar);
						uploadFileOverall.getChildren().addAll(fileImageView, uploadFileInfo);

						uploadFileOverall.setAlignment(Pos.BOTTOM_CENTER);
						uploadFileInfo.setAlignment(Pos.CENTER_LEFT);
						uploadFileOverall.setSpacing(7);

						uploadFilesGrid.add(uploadFileOverall, colIndex, rowIndex);

						colIndex++;
						if (colIndex > 1) {
							colIndex = 0;
							rowIndex++;
						}
					}
				}
			}
		});

		uploadBtn.setOnAction(event -> {
			if (selectedFiles != null) {
				LinkedList<ProgressBar> barList = new LinkedList<>();
				for (Node node : uploadFilesGrid.getChildren()) {
					if (node instanceof HBox hbox) {
						for (Node childNode : hbox.getChildren()) {
							if (childNode instanceof VBox vbox) {
								for (Node innerChildNode : vbox.getChildren()) {
									if (innerChildNode instanceof ProgressBar progressBar) {
										barList.add(progressBar);
									}
								}
							}
						}
					}
				}
				int index = 0;
				for (File file : selectedFiles) {
					CSFileUpload clientSend = new CSFileUpload(file.getPath(), username, "admin", barList.get(index));
					new Thread(clientSend).start();
					index++;
				}
			}

		});

		infoVBox.getChildren().addAll(imageView, uploadLabel);
		uploadFilesGrid.getChildren().add(infoVBox);

		uploadOverall.getChildren().addAll(uploadSeparator, uploadScrollPane, uploadBtn);


		uploadTab.setContent(uploadOverall);
		return uploadTab;
	}

	private static void setFilesInfoGrid(GridPane infoTypeFilesGrid, List<String> fileInfoList, String username, String type) {
		int colIndex = 0;
		int rowIndex = 0;
		for (int i = 0; i < fileInfoList.size() && fileInfoList.size() != 1; i = i + 3) {
			String fileName = fileInfoList.get(i);

			HBox fileOverall = new HBox();

			HBox hboxC = new HBox();
			hboxC.setSpacing(3);

			fileOverall.setPadding(new Insets(7.7));
			fileOverall.setStyle("-fx-background-color:#CDCDCD30;" + "-fx-border-style: solid inside;"
					+ "-fx-border-width: 3px;" + "-fx-border-radius: 5px;" + "-fx-border-color: #CDCDCD;");

			VBox fileInfo = new VBox();
			fileInfo.setSpacing(7);
			fileInfo.setMaxWidth(370);
			fileInfo.setMaxHeight(89);

			Label fileNameLabel = new Label(fileName);
			fileNameLabel.setFont(new Font("Yu Gothic Medium", 12));
			fileNameLabel.setMaxWidth(200);
			fileNameLabel.setTranslateX(1);

			Tooltip.install(fileNameLabel, new Tooltip(fileName));

			Label uploadTimeLabel = new Label("上传时间：" + fileInfoList.get(i + 2));
			uploadTimeLabel.setFont(new Font("Yu Gothic Medium", 12));
			uploadTimeLabel.setMaxWidth(200);

			Label fileSizeLabel = new Label("文件大小：" + fileInfoList.get(i + 1));
			fileSizeLabel.setFont(new Font("Yu Gothic Medium", 12));
			fileSizeLabel.setMaxWidth(200);

			ProgressBar downloadFileBar = new ProgressBar(0);
			downloadFileBar.setMinWidth(200);

			ImageView fileImageView = new ImageView(new Image(
					Objects.requireNonNull(LoginPage.class.getResource("/img/downloadFile.png")).toExternalForm()));
			fileImageView.setTranslateY(-10);

			Button btnDownload = new Button("下载文件");

			btnDownload.setOnAction(event -> {
				CRFileDownload crFileDownload = new CRFileDownload(fileName, username, type, downloadFileBar);
				new Thread(crFileDownload).start();
			});

			hboxC.getChildren().addAll(btnDownload, fileSizeLabel);
			fileInfo.getChildren().addAll(fileNameLabel, uploadTimeLabel, hboxC, downloadFileBar);
			fileOverall.getChildren().addAll(fileImageView, fileInfo);

			fileOverall.setAlignment(Pos.BOTTOM_CENTER);
			fileInfo.setAlignment(Pos.CENTER_LEFT);
			hboxC.setAlignment(Pos.CENTER_LEFT);
			fileOverall.setSpacing(7);

			infoTypeFilesGrid.add(fileOverall, colIndex, rowIndex);

			colIndex++;
			if (colIndex > 2) {
				colIndex = 0;
				rowIndex++;
			}
		}
	}

	private static Stage createAddUser(Stage userInforStage) {
		Stage primaryStage = new Stage();

		AnchorPane addUserAP = new AnchorPane();

		VBox userVBox = new VBox(20);

		VBox userVboxF = new VBox(7);
		userVboxF.setTranslateX(-3);

		VBox userVboxS = new VBox(7);
		userVboxS.setTranslateX(-2);

		userVBox.setPrefWidth(530);
		userVBox.setPrefHeight(310);
		userVBox.setTranslateY(-20);

		HBox hboxError = new HBox();
		hboxError.setAlignment(Pos.CENTER);
		ImageView imageViewError =new ImageView(new Image(
				Objects.requireNonNull(LoginPage.class.getResource("/img/dropR.png")).toExternalForm()));
		hboxError.getChildren().addAll(imageViewError, new Text());

		Text userTextF = new Text("用户名");
		userTextF.setFont(new Font(17));

		TextField username = new TextField();
		username.setMaxWidth(300);
		username.setPrefHeight(36);
		username.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"
		);
		username.setOnMouseEntered(e -> username.setStyle(
				"-fx-background-color: linear-gradient(to right, #7B68EE30, #6699CC30);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));
		username.setOnMouseExited(e -> username.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));

		Text userTextS = new Text("密码");
		userTextS.setFont(new Font(17));

		PasswordField password = new PasswordField();
		password.setMaxWidth(300);
		password.setPrefHeight(36);
		password.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"
		);
		password.setOnMouseEntered(e -> password.setStyle(
				"-fx-background-color: linear-gradient(to right, #7B68EE30, #6699CC30);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));
		password.setOnMouseExited(e -> password.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996630, #FFCC9930);"
						+ "-fx-background-radius: 10px;"
						+ "-fx-border-radius: 10px;"
						+ "-fx-border-color: #848484;"));

		Button addUserBtn = new Button("添加用户");
		addUserBtn.setPrefWidth(160);
		addUserBtn.setPrefHeight(36);
		addUserBtn.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996699, #FFCC9999);"
						+ "-fx-background-radius: 25px;"
						+ "-fx-border-radius: 25px;"
		);
		addUserBtn.setOnMouseEntered(e -> addUserBtn.setStyle(
				"-fx-background-color: linear-gradient(to right, #7B68EE99, #6699CC99);"
						+ "-fx-background-radius: 25px;"
						+ "-fx-border-radius: 25px;"));
		addUserBtn.setOnMouseExited(e -> addUserBtn.setStyle(
				"-fx-background-color: linear-gradient(to right, #FF996699, #FFCC9999);"
						+ "-fx-background-radius: 25px;"
						+ "-fx-border-radius: 25px;"));
		addUserBtn.setOnAction(e -> {
			String usernameStr = username.getText();
			String passwordStr = password.getText();
			if (!usernameStr.equals("") && !passwordStr.equals("")) {
				CSCmdProcess cmdProcess = new CSCmdProcess(usernameStr, passwordStr, "verify username exist");
				new Thread(cmdProcess).start();
				try {
					if (cmdProcess.getCmdState()) {
						primaryStage.close();
					} else {
						Text text = (Text) hboxError.getChildren().get(1);
						text.setText("输入的用户名已经存在，请重新输入！");
						username.setText("");
						password.setText("");
						if(userVBox.getChildren().indexOf(hboxError) != 3) {
							userVBox.getChildren().add(hboxError);
						}
					}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			} else {
				Text text = (Text) hboxError.getChildren().get(1);
				text.setText("请输入用户名或者密码，用户名或密码不能为空！");
				if(userVBox.getChildren().indexOf(hboxError) != 3) {
					userVBox.getChildren().add(hboxError);
				}
			}
		});

		userVboxF.setAlignment(Pos.CENTER);
		userVboxS.setAlignment(Pos.CENTER);
		userVBox.setAlignment(Pos.CENTER);

		userVboxF.getChildren().addAll(userTextF, username);
		userVboxS.getChildren().addAll(userTextS, password);
		userVBox.getChildren().addAll(userVboxF, userVboxS, addUserBtn);

		addUserAP.getChildren().add(userVBox);

		Scene addUserScene = new Scene(addUserAP);

		primaryStage.setTitle("添加用户");
		primaryStage.getIcons().add(new Image(
				Objects.requireNonNull(LoginPage.class.getResource("/img/logo.png")).toExternalForm()));
		primaryStage.setResizable(false);
		primaryStage.initOwner(userInforStage);
		primaryStage.initModality(Modality.WINDOW_MODAL);
		primaryStage.setWidth(530);
		primaryStage.setHeight(310);
		primaryStage.setScene(addUserScene);
		primaryStage.show();

		return primaryStage;
	}

}