package com.aiten.fds.main;

import com.aiten.fds.handles.server.SRCmdProcess;
import com.aiten.fds.handles.server.SRFileUpload;
import com.aiten.fds.handles.server.SSAllFileInfo;
import com.aiten.fds.handles.server.SSFileDownload;
import com.aiten.fds.handles.server.SSFileInfo;
import com.aiten.fds.handles.server.SSLoginVerify;
import com.aiten.fds.handles.server.SSUserInfo;
import com.aiten.fds.utils.DataHandleUtils;

public class ServerLaunch {

	public static void main(String[] args) {
		new Thread(DataHandleUtils::initializeFolder).start(); // 初始化服务器存储文件夹
		new Thread(() -> new SSFileDownload().startServer()).start(); // 启动下载服务器[端口号：6666]
		new Thread(() -> new SRFileUpload().startServer()).start(); // 启动上传服务器[端口号：8888]
		new Thread(() -> new SSFileInfo().startServer()).start(); // 启动获取单个相应用户文件信息服务器[端口号：7777]
		new Thread(() -> new SSUserInfo().startServer()).start(); // 启动获取用户信息服务器[端口号：9999]
		new Thread(() -> new SSLoginVerify().startServer()).start(); // 启动登录验证服务器[端口号：5555]
		new Thread(() -> new SSAllFileInfo().startServer()).start(); // 启动获取所有用户文件信息服务器[端口号：4444]
		new Thread(() -> new SRCmdProcess().startServer()).start(); // 启动启动接收用户命令执行服务器[端口号：3333]
	}

}
