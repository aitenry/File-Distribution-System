package com.aiten.fds.dao;

import java.util.List;

import com.aiten.fds.utils.OpnDBUtils;
import com.aiten.fds.domain.User;

public class UserDAO {
	
	public boolean verifyUser(String username, String password){
		User users = OpnDBUtils.queryForObject(
				User.class, "select username, password from users where username=?;", username);
		if (users == null) {
			return false;
		} else {
			return users.getUsername().equals(username) && users.getPassword().equals(password);
		}
	}
	
	public boolean verifyUsernameExist(String username) {
		return OpnDBUtils.queryForObject(
				User.class, "select username, password from users where username=?;", username) == null;
	}
	
	public List<User> getUserList() {
		return OpnDBUtils.queryForList(User.class, "select * from users;");
	}
	
	public boolean updateUserOnline(String username, int isOnline) {
		return OpnDBUtils.createGDBO("update users set online=? where username=?;", isOnline, username);
	}
	
	public boolean insertUser(String username, String password) {
		return OpnDBUtils.createGDBO("insert into users values (?, ?, ?);", username, password, 0);
	}
	
	public boolean deleteUser(String username) {
		return OpnDBUtils.createGDBO("delete from users where username=?;", username);
	}
	
}
