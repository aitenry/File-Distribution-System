package com.aiten.fds.dao;

import java.sql.SQLException;
import java.util.List;

import com.aiten.fds.domain.UploadFile;
import com.aiten.fds.utils.OpnDBUtils;

public class AdminUploadDAO {
	
	public void insertAdminUploadOverall(List<UploadFile> adminUploadList) throws SQLException {
		OpnDBUtils.insertBatch(UploadFile.class, "insert into admin_upload values(?, ?, ?, ?, ?);", adminUploadList);
	}
	
}
