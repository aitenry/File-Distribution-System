package com.aiten.fds.dao;

import com.aiten.fds.utils.OpnDBUtils;
import com.aiten.fds.domain.Admin;

public class AdminDAO {
	public boolean verifyAdmin(String username, String password){
		Admin admin = OpnDBUtils.queryForObject(
				Admin.class, "select username, password from admin where username=?;", username);
		if (admin == null) {
			return false;
		} else {
			if (admin.getUsername().equals(username) && admin.getPassword().equals(password)) {
				return true;
			}
		}
		return false;
	}
}
