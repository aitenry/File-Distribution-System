package com.aiten.fds.domain;

import java.util.List;

public class UserFile {
	
	private String username;
	private List<FileInfo> userFileList;
	
	public UserFile() {}
	
	public UserFile(String username, List<FileInfo> userFileList) {
		super();
		this.username = username;
		this.userFileList = userFileList;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public List<FileInfo> getUserFileList() {
		return userFileList;
	}
	
	public void setUserFileList(List<FileInfo> userFileList) {
		this.userFileList = userFileList;
	}

	@Override
	public String toString() {
		return "UserFile [username=" + username + ", userFileList=" + userFileList + "]";
	}
	
}
