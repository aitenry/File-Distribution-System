package com.aiten.fds.domain;

import java.util.Date;

public class UploadFile {
	
	private String username;
	private String file_name;
	private Date upload_time;
	private String address;
	private long file_size;
	
	public UploadFile() {}

	public UploadFile(String username, String file_name, Date upload_time, String address, long file_size) {
		super();
		this.username = username;
		this.file_name = file_name;
		this.upload_time = upload_time;
		this.address = address;
		this.file_size = file_size;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public Date getUpload_time() {
		return upload_time;
	}

	public void setUpload_time(Date upload_time) {
		this.upload_time = upload_time;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getFile_size() {
		return file_size;
	}

	public void setFile_size(long file_size) {
		this.file_size = file_size;
	}

	@Override
	public String toString() {
		return "UserUpload [username=" + username + ", file_name=" + file_name + ", upload_time=" + upload_time
				+ ", address=" + address + ", file_size=" + file_size + "]";
	}

}
