package com.aiten.fds.domain;

public class FileInfo {
	
	private String fileName;
	private String fileSize;
	private String creationTime;
	
	public FileInfo() {};
	
	public FileInfo(String fileName, String fileSize, String creationTime) {
		super();
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.creationTime = creationTime;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public String toString() {
		return "FileInfo [fileName=" + fileName + ", creationTime=" + creationTime
				+ ", fileSize=" + fileSize + "]";
	}
	
}
